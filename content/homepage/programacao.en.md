---
title: 'Schedule'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Wednesday (10/05)</th>
                <th class="tamanho-coluna-semana">Thursday (10/06)</th>
                <th class="tamanho-coluna-semana">Friday (10/07)</th>
                <th class="tamanho-coluna-semana">Saturday (10/08)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>8am</th>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#nmr-quantum-computing-and-desktop-quantum-computing-platforms">
                    NMR quantum computing and desktop quantum computing platforms (G. Feng)
                </td>
            </tr>
            <tr>
                <th>9am</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#about">Event opening</a>
                    <br><br>
                    <a href="#introduction-to-quantum-computing-with-qiskit">Introduction to quantum computing with Qiskit - Part 1 (Franklin)</a>
                </td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introduction-to-quantum-programming-with-ket">Introduction to quantum programming with Ket - Part 1 (Evandro)</a>
                </td>
                <td>
                    <a href="#multimarked-spatial-search-by-continuous-time-quantum-walk">
                        Multimarked spatial search by continuous-time quantum walk (Renato)
                    </a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>10am</th>
                <!-- <td></td> -->
                <!-- <td></td> -->
                <td>
                    <a href="#revisiting-semiconductor-hamiltonians-using-quantum-computers">
                        Revisiting semiconductor hamiltonians using quantum computers (Raphael)
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>11am</th>
                <td></td>
                <td></td>
                <td>
                    <a href="#hackathon">
                        Start of hackathon
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>12pm</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>1pm</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>2pm</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introduction-to-quantum-computing-with-qiskit">Introduction to quantum computing with Qiskit - Part 2 (Franklin)</a>
                </td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introduction-to-quantum-programming-with-ket">Introduction to quantum programming with Ket - Part 2 (Evandro)</a>
                </td>
                <td></td>
                <td>
                    <a href="#hackathon">End of hackathon</a>
                </td>
            </tr>
            <tr>
                <th>3pm</th>
                <!-- <td></td> -->
                <!-- <td></td> -->
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>4pm</th>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <a href="#about">Event closing</a>
                </td>
            </tr>
            <tr>
                <th>5pm</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>6pm</th>
                <td>
                    <a href="#impact-of-quantum-technologies-on-the-financial-industry">Impact of quantum technologies on the financial industry (Samuraí)</a>
                </td>
                <td>
                    <a href="#using-quantum-computing-to-solve-industry-problems">
                        Using quantum computing to solve industry problems (Alexandre)
                    </a>
                </td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 55px;
    }

    .tamanho-coluna-semana {
        min-width: 180px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>

<br>

# Talks

Wednesday (10/05)

* 6pm - [Impact of quantum technologies on the financial industry (Samuraí)](#impact-of-quantum-technologies-on-the-financial-industry)

Thursday (10/06)

* 6pm - [Using quantum computing to solve industry problems (Alexandre)](#using-quantum-computing-to-solve-industry-problems)

Friday (10/07)

* 9am - [Multimarked spatial search by continuous-time quantum walk (Renato)](#multimarked-spatial-search-by-continuous-time-quantum-walk)
* 10am - [Revisiting semiconductor hamiltonians using quantum computers (Raphael)](#revisiting-semiconductor-hamiltonians-using-quantum-computers)

<br>

# Minicourses

[**Introduction to quantum computing with Qiskit (Franklin)**](#introduction-to-quantum-computing-with-qiskit)

4h duration. 
 * Wednesday (10/05) from 9am to 11am
 * Wednesday (10/05) from 2pm to 4pm

[**Introduction to quantum programming with Ket (Evandro)**](#introduction-to-quantum-programming-with-ket)

4h duration.
* Thursday (10/06) from 9am to 11am
* Thursday (10/06) from 2pm to 4pm

[**NMR quantum computing and desktop quantum computing platforms (G. Feng)**](#nmr-quantum-computing-and-desktop-quantum-computing-platforms)

2h duration. 
 * Saturday (10/08) from 8am to 10pm