---
title: 'About'
weight: 10
---

Once again online, the V Quantum Computing Workshop brings together 
names from academia and industry in order to update the Brazilian 
community on the rapid advances in Quantum Computing. This revolution 
is based on the use of quantum mechanics to implement a new information 
processing and storage logic. Although at an embryonic stage, the 
quantum processors [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502) and [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), both 
from China, and the photonic processor [Borealis](https://www.nature.com/articles/s41586-022-04725-x) from the Canadian company Xanadu, already surpass the processing power of supercomputers 
like Summit and Sunway TaihuLight. In some cases quantum computers are 
about 10 million billion times faster than your classic simulation!

The [UFSC Quantum Computing Group](http://www.gcq.ufsc.br) invites you to participate in this 
scientific and technological revolution! We will present recent advances in the area 
through live lectures and minicourses. You will even have the opportunity to 
participate in a quantum hackathon. Below we have more information. 

<br>

### [Register!](#registration)

More information about the used platforms used to follow the workshop 
will be released soon. Be sure to make a registration. 

<br>

### How to participate

To participate in the event, just [register](#registration) and follow the 
talks live on YouTube (links in [program](#program)). During the presentations 
it will be possible to ask questions (anonymously if you prefer) and vote on 
other people's questions using the Slido platform. 
At the end of each session, the most voted questions will be asked to the speaker. 

<br>

### Previous Editions

[IV Quantum Computing Workshop (2021)](https://workshop-cq.ufsc.br/2021/)

[III Quantum Computing Workshop (2020)](https://workshop-cq.ufsc.br/2020/)

[II Quantum Computing Workshop (2019)](https://workshop-cq.ufsc.br/2019/)

[I Quantum Computing Workshop (2018)](https://workshop-cq.ufsc.br/2018/)