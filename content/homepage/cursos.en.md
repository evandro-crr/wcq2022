---
title: 'Minicourses'
weight: 40
header_menu: true
---

------------

# Introduction to quantum computing with Qiskit
[*Franklin de Lima Marquezino*](#franklin-de-lima-marquezino)

<p align='justify'>
Quantum computing exploits quantum behavior to perform more efficient processing than processing based on classical binary logic. In this mini-course we will study the foundations of quantum computing in a didactic way focusing on the quantum circuit model. We will also learn how to implement quantum algorithms and how to program quantum computers from IBM. There are no prerequisites, but some Linear Algebra and Python basics can be helpful.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/B2Nd3ef3a2w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/GKF3fsKrmow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

------------

# Introduction to quantum programming with Ket 
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

<p align='justify'>
We can use superposition and entanglement to develop applications accelerated by quantum computers to solve some problems faster than any supercomputer ever could. Although quantum computers capable of outperforming classical computers in solving real-world problems are not yet a reality, we hope they will be ready soon. Until then, we can prepare for that future by developing and testing quantum computing-accelerated solutions today. In this mini-course we will present the main concepts of quantum computing applied in the quantum programming language Ket. We expect all participants to interact during the course, expressing their doubts and testing what they have learned.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/Bae1w9SU3yk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/ncIp93O5J_g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

------------

# NMR quantum computing and desktop quantum computing platforms
[*Guanru Feng*](#guanru-feng)

<p align='justify'>
NMR was among the very first systems developed for quantum computing. Despite its limitations on scalability, a lot of pioneer research and techniques for quantum computing were first demonstrated in NMR systems. Many quantum control techniques developed in NMR can be readily applied to other quantum computing platforms.  Besides, the NMR technology is an ideal choice for building portable quantum computers. Here I will introduce the basics of NMR quantum computing, and the desktop NMR quantum computing platforms, Gemini, Gemini Mini and Triangulum designed and manufactured by SpinQ Technology.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/DYE6XFx44lU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>