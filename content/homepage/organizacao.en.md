---
title: 'Organization'
weight: 65
---

* Prof. Eduardo Inacio Duzzioni, Dr. (UFSC) - Coordinator
* Prof. Jerusa Marchi, Dr. (UFSC) - Coordinator
* Prof. Paulo Mafra, Dr. (UFSC)
* Prof. Tales Argolo Jesus, Dr. (CEFET-MG)
* Evandro Chagas Ribeiro da Rosa, Me. (Quantuloop)
* Otto Menegasso Pires, Me. (LAQCC/SENAI CIMATEC)
* Gilson Trombeta Magro (UFSC)
* Tomaz Souza Cruz (UFSC)
* Eduardo Willwock Lussi (UFSC)
* Gabriel Medeiros Lopes (UFSC)
* César Freitas (UFSC)
* Letícia Bertuzzi (UFSC)