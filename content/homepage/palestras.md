---
title: 'Palestras'
weight: 30
header_menu: true
---

-------------------------------
# Impacto da tecnologias quânticas na indústria financeira

[*Samuraí Brito*](#samuraí-brito)

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/arv_JJvA6eo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<p align="justify">
As tecnologias quânticas 2.0 transformarão o mundo tecnológico que conhecemos hoje. A computação quântica tem se mostrado uma grande aliada para alguns problemas complexos em finanças, no entanto, por trás de todo potencial computacional tem existe um risco à segurança da informação. Como a indústria financeira tem se preparado para essa revolução?
</p>

-----------------------------------

# Usando a computação quântica para resolver problemas de interesse da indústria

[*Alexandre de Souza*](#alexandre-de-souza)

<p align="justify">
Neste seminário vamos apresentar nossa experiência utilizando os computadores quânticos da IBM, Rigetti e Dwave para resolver alguns problemas de física com interesse da indústria. Particularmente vamos discutir a aplicação de computadores quânticos em inversão sísmica e química quântica.  Também discutiremos as fontes de erro encontradas nos computadores quânticos e métodos para mitigar tais erros.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/8NmLbZHnunQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Busca espacial multimarcada por caminhada quântica contínua no tempo

[*Renato Portugal*](#renato-portugal)

<p align="justify">
O problema da caminhada quântica em problemas de busca espacial consiste em encontrar um vértice marcado utilizando a caminhada quântica em um grafo com vértices marcados. Desenvolvemos um framework para determinar a complexidade computacional da busca espacial por caminhada quântica contínua no tempo em grafos arbitrários fornecendo uma receita para encontrar o tempo de execução ótimo e a probabilidade de sucesso do algoritmo. A caminhada quântica é conduzida por um Hamiltoniano obtido por meio da matriz de adjacências do grafo modificado pela presenta de vérticas marcados. O sucesso do nosso framework depende do conhecimento dos autovalores e autovetores da matriz de adjacências. O espectro do Hamiltoniano alterado é então obtido através da raíz do determinante de uma matriz simétrica real, cuja dimensão depende do número de vértices marcados. Nós mostramos todas etapas de funcionamento do framework na resolução de um problema de busca espacial dos grafos de Johnson com diâmetro fixo e dois vérticas marcados. Nossos cálculos mostram que o tempo de execução ótimo é O(N^(1/2)) com probabilidade assintótica de 1+o(1), onde N corresponde ao número de vértices.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/xWFpvnwHbHs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Revisitando hamiltonianos de semicondutores utilizando computadores quânticos

[*Raphael César de Souza Pimenta*](#raphael-césar-de-souza-pimenta)

<p align="justify">
A promessa de uma quebra de paradigmas em processamento de dados com o advento da computação quântica tem gerado grande comoção em toda sociedade científica. É fato que os computadores quânticos já são realidade e que pesquisas são publicadas diariamente de alguma forma os envolvendo, com um interesse crescente de instituições por todo o mundo. Com as máquinas que temos acesso nos dias de hoje, poderíamos então colocar em prática soluções úteis para pesquisas de tecnologia? Nesta palestra, pretendo discutir alguns dados levantados em minha pesquisa do uso de computadores e simuladores quânticos para solução de hamiltonianos de interesse para a ciência de materiais.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/yoURKLmZ6LM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
