---
title: 'Hackathon'
weight: 21
header_menu: true
---

Você já montou a sua equipe? O hackathon de computação quântica está chegando!

As equipes serão compostas por 3 membros. Serão 24 horas de pura adrenalina!

Maiores detalhes sobre a dinâmica do evento serão enviados por e-mail. [Inscreva-se!](https://forms.gle/iXmCHnRQgafXJgrC9)