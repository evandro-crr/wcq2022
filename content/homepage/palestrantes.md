---
title: 'Palestrantes'
weight: 50
header_menu: true
---

--------------------------------------

# Alexandre de Souza
*Centro Brasileiro de Pesquisas Físicas (Brasil)*

![Alexandre de Souza](fotos/Alexandre.png)

<p align="justify">
Alexandre é graduado em Física pela Universidade Federal Fluminense (2000), mestrado (2003) e doutorado (2008) pelo Centro Brasileiro de Pesquisas Físicas (CBPF) e pós-doutorados na Technische Universität Dortmund (Alemanha) e no Institute for Quantum Computing (IQC) da University of Waterloo (Canadá). Atualmente é pesquisador do CBPF atuando na área de Física, com ênfase em Informação Quântica e Ressonância Magnética Nuclear. 
</p>

--------------------------------------

# Evandro C. R. da Rosa
*Quantuloop (Brasil)*

![Evandro C. R. da Rosa](fotos/evandro.jpg)

<p align="justify">
Evandro é co-fundador da Quantuloop, start-up brasileira de computação quântica, e membro do Grupo de Computação Quântica da UFSC (GCQ-UFSC) desde sua criação. Mestre em Ciência da Computação pela Universidade Federal de Santa Catarina (UFSC), em sua dissertação, Evandro desenvolveu uma técnica para interação dinâmica de dados clássicos e quânticos entre computadores clássicos e computadores quânticos em nuvem, trabalho que resultou no projeto de open-source Ket. Sua área de pesquisa abrange programação e simulação quântica, criando ferramentas para facilitar o desenvolvimento de aplicações quânticas e métodos para reduzir o tempo e uso de memória na simulação de computadores quânticos.
</p>

--------------------------------------

# Guanru Feng
*SpinQ (China)*

![Guanru Feng](fotos/feng.png)
<p align='justify'>
Ph.D. em física pela Universidade de Tsinghua, fez seu pós-doutorado no IQC, Universidade de Waterloo, e atuou como técnica de pesquisas em dispositivos quânticos também no ICQ, Universidade de Waterloo. Atualmente, ela é Cientista Sênior na Shenzhen SpinQ Technology Company Limited.
</p>

--------------------------------------

# Franklin de Lima Marquezino
*Universidade Federal do Rio de Janeiro (Brasil)*

![Franklin de Lima Marquezino](fotos/Franklin.png)


<p align="justify">
Franklin é professor da Universidade Federal do Rio de Janeiro, na COPPE/Sistemas e no Campus Duque de Caxias, desde 2011; co-autor do livro "A Primer on Quantum Computing", da editora Springer; Membro do Editorial Board do periódico Theoretical Computer Science (Track C - Natural Computing); Vencedor do Prêmio CAPES de Tese 2011 na área Interdisciplinar, prêmio concedido pelas contribuições de sua tese de doutorado na área de passeios quânticos e computação quântica e Membro do programa Qiskit Advocates da IBM desde 2020. Obteve os títulos de Doutor em Modelagem Computacional pelo Laboratório Nacional de Computação Científica (LNCC) em 2010; Mestre em Modelagem Computacional pelo LNCC em 2006 e Bacharel em Ciência da Computação pela Universidade Católica de Petrópolis (UCP) em 2004. Após o doutoramento, antes de ingressar no corpo docente da UFRJ, esteve um ano como pesquisador postdoc no LNCC. Permanece ativo na área de computação quântica teórica, com ênfase em algoritmos e passeios quânticos.
</p>


--------------------------------------

# Raphael César de Souza Pimenta
*Universidade Federal de Santa Catarina (Brasil)*

![Raphael César de Souza Pimenta](fotos/raphael.png)

<p align="justify">
Raphael possui graduação em Licenciatura em Física pela Universidade Federal de Alfenas (2020), mestrado em Física pela Universidade Federal de Alfenas (2022) e atualmente é estudante de doutorado pela Universidade Federal de Santa Catarina, na área de Óptica Quântica.
</p>

--------------------------------------

# Renato Portugal
*Laboratório Nacional de Computação Científica (Brasil)*

![Renato Portugal](fotos/renato.jpg)


<p align="justify">
Prof. Renato Portugal é pesquisador Titular do LNCC/MCTI atuando na área de Computação Quântica. Ele possui Bacharelado em Física pela PUC-Rio (1981), mestrado (1984) e doutorado (1988) em Física pelo Centro Brasileiro de Pesquisas Físicas (CBPF). Realizou pós-doutoramento na Universidade de Waterloo e na Queen's University at Kingston no Canadá. Atualmente é Cientista do Nosso Estado pela Faperj, pesquisador 1D do CNPq, sócio efetivo da SBC e da SBMAC. Já orientou 17 dissertações de mestrado (uma delas ganhou o prêmio SBMAC), 13 teses de doutorado (uma delas ganhou o prêmio Capes) e supervisionou atividade de pós-doutorado no LNCC de pelo menos 14 pesquisadores nas áreas de computação quântica e computação algébrica. Foi o coordenador geral do III Workshop-Escola de Computação e Informação Quântica e publicou o livro 'Quantum Walks and Search Algorithms' pela Springer. Publicou mais de 120 trabalhos científicos completos em revistas e anais, um deles vencedor do prêmio 'Howard E. Brandt Best Paper' da Springer-Nature. Atualmente tem atuado principalmente nos seguintes temas: algoritmos de computação quântica, análise e simulação de passeios quânticos e códigos quânticos de correção de erros.
</p>

--------------------------------------



<!-- # Román Órus
*Multiverse (Espanha)*

![Román Órus](fotos/roman.png)

<p align="justify">
Román Orús é professor de pesquisa Ikerbasque no Donostia International Physics Center (DIPC) em San Sebastián (Espanha), bem como cofundador e diretor científico da Multiverse Computing, a maior empresa de software quântico da União Europeia.
</p>



-------------------------------------- -->

# Samuraí Brito
*Itaú (Brasil)*

![Samuraí Brito](fotos/samurai.jpg)

<p align="justify">
Sou mulher, casada, tenho dois filhos (Samara 8, Davi 16) e adoro tecnologia e inovação. Moro em Natal-RN e estou trabalhando no Itaú desde janeiro deste 2021. Tenho graduação, mestrado e doutorado em Física pela Universidade Federal do Rio Grande do Norte (UFRN) e Pós doutorado no Instituto Internacional de Física da UFRN na área de Informação quântica.  Atualmente lidero a frente de tecnologias quânticas do Itaú, desenhando a estratégia e posicionamento do Itaú em relação essas novas tecnologias.
</p>