---
title: 'Minicursos'
weight: 40
header_menu: true
---

------------

# Introdução à computação quântica com Qiskit
[*Franklin de Lima Marquezino*](#franklin-de-lima-marquezino)

<p align='justify'>
A computação quântica explora o comportamento quântico para realizar um processamento mais eficiente do que o processamento baseado na lógica binária clássica. Neste minicurso vamos estudar as bases da computação quântica de forma didática com foco no modelo de circuitos quânticos. Também vamos aprender como implementar algoritmos quânticos e como programar computadores quânticos da IBM. Não há pré-requisitos, porém algumas noções básicas de Álgebra Linear e de Python podem ser úteis.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/B2Nd3ef3a2w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/GKF3fsKrmow" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

------------

# Introdução à programação quântica com Ket 
[*Evandro C. R. da Rosa*](#evandro-c-r-da-rosa)

<p align='justify'>
Podemos usar superposição e emaranhamento para desenvolver aplicações aceleradas por computadores quânticos para resolver alguns problemas mais rápido do que qualquer supercomputador jamais poderia. Embora computadores quânticos capazes de superar computadores clássicos na resolução de problemas do mundo real ainda não sejam uma realidade, esperamos que eles estejam prontos em breve. Até lá, já podemos nos preparar para esse futuro, desenvolvendo e testando soluções aceleradas pela computação quântica hoje. Neste minicurso apresentaremos os principais conceitos da computação quântica aplicadas na linguagem de programação quântica Ket. Esperamos que todos os participantes interajam durante o curso, manifestando suas dúvidas e testando o que aprenderam.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/Bae1w9SU3yk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/ncIp93O5J_g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

------------

# Computação quântica NMR e plataformas de computação quântica desktop
[*Guanru Feng*](#guanru-feng)

<p align='justify'>
A NMR foi um dos primeiros sistemas desenvolvidos para computação quântica. Apesar de suas limitações de escalabilidade, muitas pesquisas e técnicas pioneiras para computação quântica foram demonstradas pela primeira vez em sistemas de NMR. Muitas técnicas de controle quântico desenvolvidas em NMR podem ser prontamente aplicadas a outras plataformas de computação quântica. Além disso, a tecnologia NMR é uma escolha ideal para construir computadores quânticos portáteis. Aqui, serão apresentados os fundamentos da computação quântica de NMR e as plataformas de computação quântica de NMR de desktop Gemini, Gemini Mini e Triangulum, projetadas e fabricadas pela SpinQ Technology.

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/DYE6XFx44lU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>