---
title: 'Sobre'
weight: 10
---

Mais uma vez online, o V Workshop de Computação Quântica reúne nomes da academia e indústria com o objetivo de atualizar a comunidade brasileira sobre os rápidos avanços da Computação Quântica. Tal revolução baseia-se no uso da mecânica quântica para implementar uma nova lógica de processamento e armazenamento de informação. Embora em estágio embrionário, os processadores quânticos [Jiuzhang](https://www.science.org/doi/10.1126/science.abe8770), [Jiuzhang 2.0](https://doi.org/10.1103/PhysRevLett.127.180502) e [Zuchongzhi](https://doi.org/10.1103/PhysRevLett.127.180501), todos da China, e o processador fotônico [Borealis](https://www.nature.com/articles/s41586-022-04725-x) da empresa canadense Xanadu, já superam o poder de processamento de supercomputadores como o Summit e o Sunway TaihuLight. Em alguns casos os computadores quânticos são cerca de 10 milhões de bilhões de vezes mais rápidos do que a sua simulação clássica exata!

O [Grupo de Computação Quântica da UFSC](http://www.gcq.ufsc.br/) convida você a participar desta revolução científica e tecnológica! Apresentaremos os recentes avanços na área através de palestras e minicursos. Você ainda terá a oportunidade de participar de um hackathon quântico. 
Abaixo temos mais informações.

<br>

### [Inscreva-se!](#inscrição) (Gratuito)

Mais informações sobre as plataformas usadas para acompanhar o workshop
serão divulgadas em breve. Não deixe de se inscrever.

<br>

### Como participar

Para participar do evento basta se [inscrever](#inscrição) e acompanhar as palestras ao vivo pelo YouTube. Durante as apresentações será possível fazer perguntas (inclusive de maneira anônima) e votar em perguntas de outras pessoas utilizando a plataforma Slido. Ao final de cada sessão, as perguntas mais votadas serão feitas ao palestrante. 

<br>

### Edições Anteriores

[IV Workshop de Computação Quântica (2021)](https://workshop-cq.ufsc.br/2021/)

[III Workshop de Computação Quântica (2020)](https://workshop-cq.ufsc.br/2020/)

[II Workshop de Computação Quântica (2019)](https://workshop-cq.ufsc.br/2019/)

[I Workshop de Computação Quântica (2018)](https://workshop-cq.ufsc.br/2018/)