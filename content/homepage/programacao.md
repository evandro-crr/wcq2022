---
title: 'Programação'
weight: 20
header_menu: true
---

<div style="overflow: auto;">
    <table class="table programacao" style="color: black; margin-bottom: 0px;">
        <thead class="">
            <tr>
                <th class="tamanho-coluna-horas"></th>
                <th class="tamanho-coluna-semana">Quarta-feira (05/10)</th>
                <th class="tamanho-coluna-semana">Quinta-feira (06/10)</th>
                <th class="tamanho-coluna-semana">Sexta-feira (07/10)</th>
                <th class="tamanho-coluna-semana">Sábado (08/10)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>8h</th>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#computação-quântica-nmr-e-plataformas-de-computação-quântica-desktop">
                    Computação quântica NMR e plataformas de computação quântica desktop (G. Feng)
                </td>
            </tr>
            <tr>
                <th>9h</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#sobre">Abertura do Evento</a>
                    <br><br>
                    <a href="#introdução-à-computação-quântica-com-qiskit">Introdução à computação quântica com Qiskit - Parte 1 (Franklin)</a>
                </td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introdução-à-programação-quântica-com-ket">Introdução à programação quântica com Ket - Parte 1 (Evandro)</a>
                </td>
                <td>
                    <a href="#busca-espacial-multimarcada-por-caminhada-quântica-contínua-no-tempo">
                        Busca espacial multimarcada por caminhada quântica contínua no tempo (Renato)
                    </a>
                </td>
                <!-- <td></td> -->
            </tr>
            <tr>
                <th>10h</th>
                <!-- <td></td> -->
                <!-- <td></td> -->
                <td>
                    <a href="#revisitando-hamiltonianos-de-semicondutores-utilizando-computadores-quânticos">
                        Revisitando hamiltonianos de semicondutores utilizando computadores quânticos (Raphael)
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>11h</th>
                <td></td>
                <td></td>
                <td>
                    <a href="#hackathon">
                        Inicio do Hackathon
                    </a>
                </td>
                <td></td>
            </tr>
            <tr>
                <th>12h</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>13h</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>14h</th>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introdução-à-computação-quântica-com-qiskit">Introdução à computação quântica com Qiskit - Parte 2 (Franklin)</a>
                </td>
                <td rowspan="2" style="vertical-align: middle;">
                    <a href="#introdução-à-programação-quântica-com-ket">Introdução à programação quântica com Ket - Parte 2 (Evandro)</a>
                </td>
                <td></td>
                <td>
                    <a href="#hackathon">Fim do Hackathon</a>
                </td>
            </tr>
            <tr>
                <th>15h</th>
                <!-- <td></td> -->
                <!-- <td></td> -->
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>16h</th>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <a href="#sobre">Fechamento do Evento</a>
                </td>
            </tr>
            <tr>
                <th>17h</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>18h</th>
                <td>
                    <a href="#impacto-da-tecnologias-quânticas-na-indústria-financeira">Impacto da tecnologias quânticas na indústria financeira (Samuraí)</a>
                </td>
                <td>
                    <a href="#usando-a-computação-quântica-para-resolver-problemas-de-interesse-da-indústria">
                        Usando a computação quântica para resolver problemas de interesse da indústria (Alexandre)
                    </a>
                </td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<style>

    .tamanho-coluna-horas {
        min-width: 35px;
    }

    .tamanho-coluna-semana {
        min-width: 180px;
    }

    .programacao > thead > tr > th, .programacao > tbody > tr > th {
        background-color: #26a69a !important;
        border: 0 !important;
    }

    td {
        border: 0 !important;
        vertical-align: middle !important;
        word-wrap: normal !important;
    }

    td > a {
        text-decoration: none;
    }

    td > a:hover {
        color: black;
        font-weight: bold;
    }

    table tbody > tr:nth-child(odd) > td, table tbody > tr:nth-child(odd) > th {
        background-color: white;
    }

    table tbody > tr:nth-child(even) > td, table tbody > tr:nth-child(even) > th {
        background-color: #ddf2f0;
    }

</style>

<br>

# Palestras

Quarta-feira (05/10)

* 18h - [Impacto da tecnologias quânticas na indústria financeira (Samuraí)](#impacto-da-tecnologias-quânticas-na-indústria-financeira)

Quinta-feira (06/10)

* 18h - [Usando a computação quântica para resolver problemas de interesse da indústria (Alexandre)](#usando-a-computação-quântica-para-resolver-problemas-de-interesse-da-indústria)

Sexta-feira (07/10)

* 9h - [Busca espacial multimarcada por caminhada quântica contínua no tempo (Renato)](#busca-espacial-multimarcada-por-caminhada-quântica-contínua-no-tempo)
* 10h - [Revisitando hamiltonianos de semicondutores utilizando computadores quânticos (Raphael)](#revisitando-hamiltonianos-de-semicondutores-utilizando-computadores-quânticos)

<br>

# Minicursos

[**Introdução à computação quântica com Qiskit (Franklin)**](#introdução-à-computação-quântica-com-qiskit)

Minicurso de 4h. 
 * Quarta-feira (05/10) das 9h as 11h
 * Quarta-feira (05/10) das 14h as 16h

[**Introdução à programação quântica com Ket (Evandro)**](##introdução-à-programação-quântica-com-ket)

Minicurso de 4h.
* Quinta-feira (06/10) das 9h as 11h
* Quinta-feira (06/10) das 14h as 16h

[**Computação quântica NMR e plataformas de computação quântica desktop (G. Feng)**](#computação-quântica-nmr-e-plataformas-de-computação-quântica-desktop)

Minicurso de 2h. 
 * Sábado (08/10) das 8h as 10h
