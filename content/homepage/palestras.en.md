---
title: 'Talks'
weight: 30
header_menu: true
---

-------------------------------

# Impact of quantum technologies on the financial industry

[*Samuraí Brito*](#samuraí-brito)

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/arv_JJvA6eo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<p align="justify">
Quantum Technologies 2.0 will transform the technological world we know today. Quantum computing has proved to be a great ally for some complex problems in finance, however, behind all computational potential there is an information security risk. How has the financial industry prepared itself for this revolution?
</p>

---------------------------------

# Using quantum computing to solve industry problems

[*Alexandre de Souza*](#alexandre-de-souza)

<p align="justify">
In this seminar we will present our experience using the quantum computers from IBM, Rigetti and Dwave to solve some physics problems of interest to the industry. Particularly, we will discuss the application of quantum computers in seismic inversion and quantum chemistry. We will also discuss sources of error found in quantum computers and methods for mitigating such errors.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/8NmLbZHnunQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Multimarked spatial search by continuous-time quantum walk

[*Renato Portugal*](#renato-portugal)

<p align="justify">
The quantum-walk-based spatial search problem aims to find a marked vertex using a quantum walk on a graph with marked vertices. We describe a framework for determining the computational complexity of spatial search by continuous-time quantum walk on arbitrary graphs by providing a recipe for finding the optimal running time and the success probability of the algorithm. The quantum walk is driven by a Hamiltonian that is obtained from the adjacency matrix of the graph modified by the presence of the marked vertices. The success of our framework depends on the knowledge of the eigenvalues and eigenvectors of the adjacency matrix. The spectrum of the modified Hamiltonian is then obtained from the roots of the determinant of a real symmetric matrix, whose dimension depends on the number of marked vertices. We show all steps of the framework by solving the spatial searching problem on the Johnson graphs with fixed diameter and with two marked vertices. Our calculations show that the optimal running time is O(N^(1/2)) with asymptotic probability 1+o(1), where N is the number of vertices.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/xWFpvnwHbHs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

-------------------------------

# Revisiting semiconductor hamiltonians using quantum computers

[*Raphael César de Souza Pimenta*](#raphael-césar-de-souza-pimenta)

<p align="justify">
The promise of a paradigm shift in data processing with the advent of quantum computing has generated great commotion throughout scientific society. It is a fact that quantum computers are already a reality and that research is published daily in some way involving them, with a growing interest from institutions all over the world. With the machines we have access to today, could we then put into practice useful solutions for technology research? In this lecture, I intend to discuss some data collected in my research on the use of computers and quantum simulators to solve Hamiltonians of interest to materials science.
</p>

<iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/yoURKLmZ6LM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
