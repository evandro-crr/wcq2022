---
title: 'Hackathon'
weight: 21
header_menu: true
---

Have you ever compose your team? The quantum computing hackathon is coming!

The teams will be composed of 3 members. It will be 24 hours of pure adrenaline!

Further details about the event’s dynamics will be sent by email. [Sign up!](https://forms.gle/iXmCHnRQgafXJgrC9)