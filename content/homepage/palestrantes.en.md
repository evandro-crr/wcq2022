---
title: 'Speakers'
weight: 50
header_menu: true 
---

--------------------------------------

# Alexandre de Souza
*Brazilian Center for Physical Research (Brazil)*

![Alexandre de Souza](../fotos/Alexandre.png)

<p align="justify">
Alexandre holds a degree in Physics from Universidade Federal Fluminense (2000), a master’s degree (2003) and a doctorate (2008) from the Brazilian Center for Physics Research (CBPF) and post-doctoral degrees from the Technische Universität Dortmund (Germany) and the Institute for Quantum Computing (IQC) from the University of Waterloo (Canada). He is currently a researcher at CBPF working in the area of Physics, with emphasis on Quantum Information and Nuclear Magnetic Resonance.
</p>

--------------------------------------

# Evandro C. R. da Rosa
*Quantuloop (Brazil)*

![Evandro Chagas](../fotos/evandro.jpg)

<p align="justify">
Evandro is a co-founder of Quantuloop, a Brazilian quantum computing start-up, and a member of the UFSC Quantum Computing Group (GCQ-UFSC) since its inception. Master in Computer Science from the Federal University of Santa Catarina (UFSC), in his dissertation, Evandro developed a technique for dynamic interaction of classical and quantum data between classical computers and quantum computers in the cloud, work that resulted in the open-source project Ket . His area of research encompasses quantum programming and simulation, creating tools to facilitate the development of quantum applications and methods to reduce time and memory usage in the simulation of quantum computers.
<p align="justify">

--------------------------------------

# Guanru Feng
*SpinQ (China)*

![Guanru Feng](../fotos/feng.png)
<p align='justify'>
Ph.D. in physics by the Tsinghua University. She did Postdoctoral research in IQC, University of Waterloo and Quantum Device research Technician in IQC, University of Waterloo. Currently, she is Senior Scientist at the Shenzhen SpinQ Technology Company Limited.
</p>

--------------------------------------

# Franklin de Lima Marquezino
*Federal University of Rio de Janeiro (Brazil)*

![Franklin de Lima Marquezino](../fotos/Franklin.png)

<p align="justify">
Frankling is a professor at Federal University of Rio de Janeiro, at COPPE/Systems and Duque de Caxias Campus, since 2011; co-author of the book "A Primer on Quantum Computing", by Springer; Member of Editorial Board of Theoretical Computer Science (Track C - Natural Computing); Winner of the 2011 CAPES Thesis Award in the Interdisciplinary area, awarded for the contributions of his doctoral thesis in the area of quantum walks and quantum computing and Member of IBM's Qiskit Advocates program since 2022. He obtained the titles of Doctor in Computational Modeling by the National Laboratory of Scientific Computing (LNCC) in 2010; Master in Computing Modeling from LNCC in 2006 and Bachelor in Computer Science from the Catholic University of Petrópolis (UCP) in 2004. After his PhD, before joining faculty at UFRJ, he spent a year as postdoc researcher at LNCC. He remais active in the field of theoretical quantum computing, with an emphasis on algorithms and quantum walks.
</p>


--------------------------------------

# Raphael César de Souza Pimenta
*Federal University of Santa Catarina (Brazil)*

![Raphael César de Souza Pimenta](../fotos/raphael.png)

<p align="justify">
Raphael holds a degree in Physics from the Federal University of Alfenas (2020), a master's degree in Physics from the Federal University of Alfenas (2022) and is currently a doctoral student at the Federal University of Santa Catarina, in the area of Quantum Optics.
</p>

--------------------------------------

# Renato Portugal
*National Laboratory of Scientific Computing (Brazil)*

![Renato Portugal](../fotos/renato.jpg)


<p align="justify">
Prof. Renato Portugal is a Researcher at LNCC/MCTI working in the field of Quantum Computing. He holds a bachelor's degree in Physics from PUC-Rio (1981), a master's degree (1984) and Ph.D. (1988) in Physics from the Brazilian Center for Physics Research (CBPF). He visited during his sabbatical years the University of Waterloo and Queen's University at Kingston in Canada. Currently, he is a Cientista-do-nosso-Estado by Faperj, holds a productivity grant 1D from CNPq, and is a member of SBC and SBMAC societies. He has supervised 17 master's dissertations (one of them won the SBMAC prize), 13 doctoral theses (one of them won the Capes prize) and supervised postdoctoral activity at the LNCC of at least 14 researchers in the areas of quantum computing and computer algebra. He was the chair of the III Workshop-School of Quantum Computation and Information and published the book Quantum Walks and Search Algorithms by Springer. He has published over 120 full-length scientific papers in journals and annals, one of which won the Springer-Nature Howard E. Brandt Best Paper award. Currently, he is working mainly on the following topics: quantum algorithms, analysis and simulation of quantum walks, and quantum error-correcting codes.
</p>


--------------------------------------

<!-- # Román Órus
*Multiverse (Spain)*

![Román Órus](../fotos/roman.png)

<p align="justify">
Román Orús is Ikerbasque Research Professor at the Donostia International Physics Center (DIPC) in San Sebastián (Spain) as well as Co-founder and Chief Scientific Officer of Multiverse Computing, the largest quantum software company in the European Union. 
</p>


-------------------------------------- -->

# Samuraí Brito
*Itaú (Brazil)*

![Samuraí Brito](../fotos/samurai.jpg)

<p align="justify">
I am a woman, married, I have two children (Samara 8, Davi 16) and I love technology and innovation. I live in Natal-RN and have been working at Itaú since January 2021. I have a degree, master's and doctorate degree in Physics from the Federal University of Rio Grande do Norte (UFRN) and Post-Doctorate at the International Institute of Physics at UFRN in the area of Quantum Information. Currently, I lead Itaú's quantum technologies front, designing Itaú's strategy and positioning in relation to these new technologies.
</p>
